__author__ = 'Sealab'

from Parser.AST import *
from Parser.Lexer import *
import os

class Game:
    def __init__(self):
        self.master = {}    # Master config file
        self.save   = {}    # Character's save data
        self.rooms  = {}    # The rooms that the game is using
        self.dataFiles = [] # List of data files that have been loaded
        self.loopGame = False
        self.readMasterFile()

        self.dispatcher = {}
        self.dispatcher["Print"]    = self._print
        self.dispatcher["If"]       = self._if
        self.dispatcher["SetVar"]   = self._setVar
        self.dispatcher["GetVar"]   = self._getVar

        # Load in the game's data files
        self.loadDataFile(self.master["mainDataFile"])

        # Load or create a save
        self.loadOrCreate()

        # Then start the game
        self.loop()

    ### Main Game logic START ###
    def loop(self):
        self.loopGame = True

        # Keep looping until we're told to stop
        while self.loopGame:
            command = self.parseCommand(input("Enter command: "))

            # Some predefined commands
            if command == "exit":
                self.loopGame = False
                continue

            # Call the room's hook for the command(if it's there)
            room = self.rooms[self.save["Room"]]
            if self.roomHasHook(room, command):
                self.executeHook(self.getHookFromRoom(room, command))
            else:
                print("Unknown command")

            # Formatting
            print("")

    def parseCommand(self, command):
        """
        Parses the given command string
        :param command: The command to parse
        :return: The hook to call for this command
        """

        # Format = command param1 param2...
        data = command.split(' ')

        # Set the variables for the room hooks
        self.setVar("ParamCount", len(data) - 1)

        # Param1, Param2, etc.
        if len(data) != 1:
            for i in range(1, len(data)):
                self.setVar("Param" + str(i), data[i])

        return data[0]
    ### Main Game logic END ###

    ### Util START ###
    def __str__(self):
        string = ""
        string += "Master: " + str(self.master) + "\n"
        string += "Save: " + str(self.save) + "\n"
        string += "Rooms: " + str(self.rooms) + "\n"
        string += "Data Files: " + str(self.dataFiles) + "\n"

        return string

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # This is called if we use a with statement
        self.saveGame()

    def readMasterFile(self):
        """
        Reads in the master config file.
        """
        lines = [line.rstrip('\n') for line in open("Data/Master.txt")]

        # Key=Value
        for line in lines:
            if "=" in line:
                data = line.split("=", 1)
                self.master[data[0]] = data[1]
    ### Util END ###

    ### Variable functions START ###
    def setVar(self, varName, value):
        """
        Sets the value of varName to value
        :param varName: The name of the varaible to set the value of
        :param value: The value to set
        """
        self.save["Variables"][varName] = value

    def getVar(self, varName):
        """
        :param varName: The name of the variable to get the value of
        :return: The value of the variable
        """
        if not varName in self.save["Variables"]:
            return None

        return self.save["Variables"][varName]
    ### Variable functions END ###

    ### Game save functions START ###
    def loadOrCreate(self):
        # Clear the screen and ask the player what they want to do
        # We loop incase they enter invalid input
        while True:
            self.clearScreen()

            print("Enter option:")
            print("\t1. Create new game")
            print("\t2. Load saved game")
            answer = input()

            if answer == "1":
                self.newGame()
                break
            elif answer == "2":
                pass
                break

    def newGame(self):
        # Clear the screen, and then setup the save data
        self.clearScreen()
        self.save["Name"]       = input("Enter your character's name: ")
        self.save["Inventory"]  = {}
        self.save["Variables"]  = {} # Variables that the rooms can use
        self.save["Room"]       = self.master["startRoom"]

    def saveGame(self):
        pass
    ### Game save functions END ###

    ### Player's inventory methods START ###
    def addItem(self, itemName, count):
        """
        Adds the given item to the player's inventory.
        :param itemName: The item to add
        :param count: How many of the item there is
        """

        # If the item is already there, just add the counts together
        if self.hasItem(itemName):
            self.save["Inventory"][itemName] += count
        else:
            self.save["Inventory"][itemName] = count

    def hasItem(self, itemName):
        """
        :param itemName: The item to check for
        :return Whether or not the item is in the player's inventory:
        """
        return itemName in self.save["Inventory"]

    def getItemCount(self, itemName):
        """
        :param itemName: The item to get the count of
        :return: How many of the wanted item there are in the player's inventory
        """
        if self.hasItem(itemName):
            return 0
        else:
            return self.save["Inventory"][itemName]
     ### Player's inventory methods END ###

    ### Game data functions START ###
    def loadDataFile(self, file):
        """
        Loads in the specified data file
        :param file: The file to load
        """
        if file in self.dataFiles:
            return
        else:
            self.dataFiles.append(file)

        with open(file) as f:
            code = f.read()

        ast = ASTConstructor(Lexer(code))

        # Go over every node in the file
        for node in ast.mainNode.nodes:
            # Do stuff depending on what the type of the node is
            if isinstance(node, DeclarationNode):
                if node.type == DeclarationType.Room:
                    self.rooms[node.name] = node
                else:
                    raise Exception("Unhandled declaration node.", node)
            else:
                raise Exception("Unhandled node.", node)
    ### Game data functions END ###

    ### Hook execution functions START ###
    def roomHasHook(self, room: DeclarationNode, hookName):
        """
        :param room: The room to search for the hook in
        :param hookName: The hook to search for
        :return: Whether the room has a certain hook
        """

        return self.getHookFromRoom(room, hookName) is not None

    def getHookFromRoom(self, room, hookName):
        """
        :param room: The room that holds the hook
        :param hookName: The hook to retrieve
        :return: The hook from the room
        """
        for node in room.nodes:
            if node.type == DeclarationType.Hook and node.name == hookName:
                return node

        return None

    def executeHook(self, hookNode):
        """
        Executes the given hook
        :param hookNode: The hook to execute
        """

        # Go over each node in the hook
        for node in hookNode.nodes:
            # If it's a call, then run it through the excuteCall
            if isinstance(node, CallStatementNode):
                self.dispatchCall(node)
            else:
                raise Exception("Unhandled node in hook.", node)

    def dispatchCall(self, call):
        """
        Dispatches the given call to the correct function
        :param call: The call to dispatch
        """
        return self.dispatcher[call.name](call)
    ### Hook execution functions END ###

    ### Script functions START ###
    def resolveParameter(self, param):
        """
        If a CallStatementNode is passed through, then it is ran and it's value is returned
        :param param: The parameter to resolve
        """
        if isinstance(param, CallStatementNode):
            return self.dispatchCall(param)
        elif isinstance(param, Token):
            if param.type == TokenType.String:
                return param.text
            elif param.type == TokenType.Number:
                return int(param.text)
            else:
                return param
        else:
            return param

    def _print(self, call):
        string = ""
        for param in call.parameters:
            string += str(self.resolveParameter(param))

        print(string)

    def _if(self, call):
        assert len(call.parameters) == 4, "Invalid number of if parameters, expected 4. " + str(call)

        # Get the stuff for the if
        left = self.resolveParameter(call.parameters[0])

        # Make sure there is an operator
        op = call.parameters[1]
        assert isinstance(op, Token), "Internal error. Expected token for parameter 2. " + str(op)
        assert op.type == TokenType.Operator, str(op)

        right = self.resolveParameter(call.parameters[2])

        # Then see if the if is correct
        if type(left) != type(right):
            return

        correct = False
        if op.text == '=':
            correct = left == right
        elif op.text == '!':
            correct = left != right
        elif op.text == '<':
            correct = left < right
        elif op.text == '>':
            correct = left > right
        else:
            raise Exception("Invalid comparison character.", str(op))

        # If it was correct, then call it's function
        if correct:
            assert isinstance(call.parameters[3], DeclarationNode), "Internal error. Expected a declaration for last parameter" + str(call.parameters[3])
            self.executeHook(call.parameters[3])

    def _setVar(self, call):
        assert len(call.parameters) == 2, "Invalid amount of parameters for SetVar, expected 2."

        self.setVar(self.resolveParameter(call.parameters[0]), self.resolveParameter(call.parameters[1]))

    def _getVar(self, call):
        assert len(call.parameters) == 1, "Invalid amount of parameters for GetVar, expected 1."

        return self.getVar(self.resolveParameter(call.parameters[0]))
    ### Script functions END ###

    def clearScreen(self):
        os.system("cls")