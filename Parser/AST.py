__author__ = 'Sealab'

from Parser.Lexer import *
from enum import Enum
from Util.TabWriter import TabWriter


class DeclarationType(Enum):
    Hook = 1
    Room = 2
    File = 3
    Nil  = 4


class DeclarationNode:
    def __init__(self, name, type = DeclarationType.Nil):
        """
        Contains the nodes of something
        :param name: The name of the declaration
        :param type: The type of the declaration
        """
        self.nodes = []     # Contains all of the nodes of the thing
        self.name  = name   # The name of the declaration
        self.type  = type   # What kind of thing this node is describing

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        string = "DeclarationNode[Name='%s', Type=%s] Nodes:\n" % (self.name, self.type)

        TabWriter.incrememnt()
        for i in range(0, len(self.nodes)):
            string += TabWriter.entab("[%s] = %s\n" % (i, str(self.nodes[i])))

        TabWriter.decrement()
        return string

    def add(self, node):
        """
        Adds the given node to the declaration.
        :param node: The node to add
        """
        self.nodes.append(node)


class CallStatementNode:
    def __init__(self, name, parameters):
        """
        Contains data about a function call
        :param name: The name of the function to call
        :param parameters: The parameters to give the function
        """
        self.name       = name
        self.parameters = parameters

    def __str__(self):
        string = "CallStatementNode[Name='%s'] Params:\n" % self.name

        TabWriter.incrememnt()
        for i in range(0, len(self.parameters)):
            string += TabWriter.entab("\t[%s] %s\n" % (i, self.parameters[i]))

        TabWriter.decrement()
        return string


class ASTConstructor:
    def __init__(self, lexer):
        """
        Constructs an AST using the given lexer
        :param lexer: The lexer containing the code
        """
        self.mainNode = DeclarationNode("", DeclarationType.File)
        self.lexer = lexer

        while not self.lexer.isEOF():
            self.mainNode.add(self.readDeclaration())
            self.lexer.skipBlanks()

    def readDeclaration(self) -> DeclarationNode:
        toReturn = self.readDeclarationSignature()

        # Hooks have to be read in differently(Well, most of the stuff does)
        if toReturn.type == DeclarationType.Hook:
            return self.readHook(toReturn)

        while True:
            assert not self.lexer.isEOF()

            tok = self.lexer.peek()

            # If we get to the End token then remove it from the lexer and then break
            if tok.type == TokenType.Keyword and tok.text == "End":
                self.lexer.next()
                break
            else: # Otherwise, add it to the declaration(as it will be another declaration)
                toReturn.add(self.readDeclaration())

        assert toReturn.type != DeclarationType.Nil
        return toReturn

    def readDeclarationSignature(self) -> DeclarationNode:
        # Reads a declaration and returns it
        toReturn = DeclarationNode("", DeclarationType.Nil)

        # Read in the type
        tok = self.lexer.next()
        assert tok.type == TokenType.Keyword
        toReturn.type   = self.typeFromText(tok.text)

        # Make sure a : is next
        tok = self.lexer.next()
        tok.assertAll(TokenType.Operator, ':')

        # Read in the name
        tok = self.lexer.next()
        assert tok.type == TokenType.Identifier
        toReturn.name = tok.text

        # Then finally, make sure a > is last
        tok = self.lexer.next()
        tok.assertAll(TokenType.Operator, '>')

        return toReturn

    def readHook(self, hook):
        # Keep reading in function calls until we hit end
        while True:
            assert not self.lexer.isEOF()
            tok = self.lexer.peek()

            if tok.type == TokenType.Keyword and tok.text == "End":
                self.lexer.next()
                break
            else:
                hook.add(self.readCall())

        return hook

    def readCall(self):
        # First make sure there's a $
        tok = self.lexer.next()
        tok.assertAll(TokenType.Operator, '$')
        call = CallStatementNode("", [])

        # Then get the name
        tok = self.lexer.next()
        assert tok.type == TokenType.Keyword, tok
        call.name = tok.text

        # Make sure we have a ( next
        tok = self.lexer.next() # I really should make a function for these checks...
        tok.assertAll(TokenType.Operator, '(')

        gettingParameter = True

        # Then, keep reading in parameters until we hit )
        while True:
            assert not self.lexer.isEOF()
            tok = self.lexer.peek()

            if tok.text == ')' and tok.type == TokenType.Operator:
                self.lexer.next()
                break

            # If we're looking for a parameter, then just add in the token(Or parse a call statement, if there is one)
            if gettingParameter:
                if tok.type == TokenType.Operator and tok.text == '$':
                    call.parameters.append(self.readCall())
                else:
                    self.lexer.next()
                    call.parameters.append(tok)

                gettingParameter = False
            else: # Otherwise, make sure it's either a ) or ,
                assert tok.type == TokenType.Operator, tok
                gettingParameter = True
                self.lexer.next()

                assert tok.text == ','

        if call.name == "If":
            # If we're reading in an if statement, then read in another hook(ifs are basically nested hooks)
            # And put it as the last parameter. So when the condition is true, we can run the hook.

            # But first, make sure there's an >
            tok = self.lexer.next()
            tok.assertAll(TokenType.Operator, '>')

            hook = DeclarationNode("", DeclarationType.Hook)
            call.parameters.append(self.readHook(hook))

        return call

    def typeFromText(self, text):
        """
        :param text: The textual form of the type
        :return: A declaration type depending on what text is
        """

        return DeclarationType[text]