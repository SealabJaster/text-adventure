__author__ = 'Sealab'

from enum import Enum

class TokenType(Enum):
    """ Contains the different type of tokens """

    # Single character operator
    Operator    = 1

    # Certain word with a special meaning
    Keyword     = 2

    # Text that was inside speech marks
    String      = 3

    # No type
    Nil         = 4

    # Stuff like: room1, hookName, etc.
    Identifier  = 5

    # Numeric literals
    Number      = 6


class Token:
    def __init__(self, type = TokenType.Nil, text = "", line = 0):
        """
        Contains data about a token
        :param type: The type of the token
        :param text: The textual form of the token
        :param line: Which line the token was created on
        """
        self.type = type
        self.text = text
        self.line = line

    def __str__(self) -> str:
        return "Token[Type=%s, Text='%s', Line=%s]" % (self.type, self.text, self.line)

    def assertAll(self, expectedType, expectedText):
        assert self.type == expectedType and self.text == expectedText, self


class Lexer:
    def __init__(self, code):
        """
        Converts the game's description code into tokens
        :param code: The code to lex
        """
        self.code       = code  # The code to lex
        self.line       = 1     # The current line we're on
        self.index      = 0     # The index we're at in the code
        self.peeked     = False # If true, then we need to return self.peekToken next time next is called.
        self.peekToken  = Token(TokenType.Nil) # The token to return if we peeked

        # The operators for the code
        self.operators = [':', '>', '(', ')', '$', ',', '!', '=']

        # The keywords for the code
        self.keywords = ["Room", "Hook", "End", "Print", "If", "HasItem", "SetVar", "GetVar"]

    def isEOF(self) -> bool:
        """
        :return: Whether the lexer is at the end of the code or not
        """
        return (self.index >= len(self.code))

    def skipBlanks(self):
        """
        Skips over any whitespace
        """
        while True:
            if self.isEOF():
                break
            else:
                char = self.readChar(False)

                if char == '\n':
                    self.line += 1

                if not char.isspace():
                    self.index -= 1
                    break

    def peek(self) -> Token:
        """
        Reads and returns the next token without going past it in the lexer
        :return: The next token
        """
        if self.peeked:
            return self.peekToken

        self.peekToken  = self.next()
        self.peeked     = True

        return self.peekToken

    def next(self) -> Token:
        """
        Reads and returns the next token in the code
        :return: The next token
        """
        self.skipBlanks()

        if self.peeked:
            self.peeked = False
            return self.peekToken

        # If we're at the end of the code, return a Nil token
        if self.isEOF():
            return Token(TokenType.Nil)

        # Get the next character, and depending on what it is, construct a token for it
        char = self.readChar()

        # Check for comments and skip the entire line if there is one
        if char == '/':
            if self.readChar() == '/':
                while True:
                    if self.isEOF():
                        return Token(TokenType.Nil)
                    elif self.readChar() == '\n':
                        # We call this function again just to avoid bugs of rewriting the same thing as above
                        self.index -= 1
                        return self.next()
            else:
                self.index -= 1

        self.index -= 1 # We need to make sure the "nextX" functions see the character we read in.

        if char in self.operators:
            # Operators
            self.index += 1 # Stops infinite loops
            return Token(TokenType.Operator, char, self.line)
        elif char == '"':
            # Strings
            return self.nextString()
        else:
            # Identifiers, keywords, and numbers
            tok = self.nextIdentifier()

            if tok.text in self.keywords:
                tok.type = TokenType.Keyword
            elif tok.text.isnumeric():
                tok.type = TokenType.Number

            return tok

    def nextString(self) -> Token:
        """
        :return: The next string in the code
        """
        self.skipBlanks()
        tok = Token(TokenType.String, "", self.line)

        # Make sure the next character is a speech mark
        assert self.readChar() == '"', "Expected '\"' for start of string. Line=%s'" % self.line

        while True:
            if self.isEOF():
                raise Exception("Unterminated string.", "Line=%s" % self.line)

            char = self.readChar(True)
            if char == '"':
                break
            else:
                tok.text += char

        return tok

    def nextIdentifier(self) -> Token:
        """
        :return: The next identifier in the code
        """
        self.skipBlanks()
        tok = Token(TokenType.Identifier, "", self.line)

        # Keep reading in character, until we're at EoF, a whitespace char, or an operator
        while True:
            if self.isEOF():
                break

            char = self.readChar()
            if char.isspace():
                self.index -= 1
                break
            elif char in self.operators:
                self.index -= 1
                break
            else:
                tok.text += char

        return tok

    def readChar(self, allowEscapes = False) -> str:
        """
        Reads the next character in the code, parses escape character.
        :param allowEscapes: If true, parses escape characters. If false, raises an exception upon escape characters.
        :return: The next character
        """

        # Get the next character
        char = self.code[self.index]
        self.index += 1

        # If it's an escape character, parse it.
        if char == "\\":
            # Get the next character after it, and figure out from there what it is
            char = self.code[self.index]
            self.index += 1

            if not allowEscapes:
                raise Exception("Unexpected escape character.", char, "Line=%s" % self.line)

            if char == "n":
                char = '\n'
            elif char == "t":
                char = '\t'
            else:
                raise Exception("Invalid escape character", char, "Line=%s" % self.line)

        return char

# Does it currently work correctly? True(Should probably just make a unittest for this, but meh)
#
# Quickly test the lexer
# _code = '>\t\n   $Print End // This comment should be completely ignored\n"My word old chap!"'
# _lex  = Lexer(_code)
# while True:
#     _tok = _lex.next()
#
#     if _tok.type == TokenType.Nil:
#         break
#     else:
#         print(_tok)
