__author__ = 'Sealab'

class TabWriter:
    # Provides an easy way to create tabbed strings
    tabCount = 0

    @classmethod
    def incrememnt(cls):
        cls.tabCount += 1

    @classmethod
    def decrement(cls):
        cls.tabCount -= 1

    @classmethod
    def entab(cls, string):
        toReturn = '\t' * cls.tabCount

        return toReturn + string